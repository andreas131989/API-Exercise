As the API will communicate directly with the db service via cluster IP, a separate docker image is created where the host value in the NodeJS server is set to this IP.

Improvement: Use DNS name instead of IP