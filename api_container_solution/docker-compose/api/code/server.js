//Define dependencies
const express = require('express')
const app = express()
const morgan = require('morgan')
const mysql = require('mysql')
const bodyParser = require ('body-parser')

app.use(morgan('short'))

//Use body parser for POST request (translate json/text parameters)
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true })); 

//Define a connection to DB
const connection = mysql.createConnection({
      host: 'mysql',
      user: 'root',
      password: 'root',
      database: 'passengers'
})


//Http GET request depending on the passenger ID
app.get('/titanic/:ID', (req, res) => {
  console.log("Fetching user: " + req.params.ID)
 
  const userId = req.params.ID
  const queryString = "SELECT * FROM titanic WHERE ID = ?"
 
 connection.query(queryString, [userId], (err, rows, fields) => {
    
    if (err) {
      console.log("Failed: " + err)
      res.sendStatus(500)
      res.end()
      return
    }
    console.log("We fetched") 
    res.json(rows)
  }) 
})


//Http POST request. Currently, the data are not imported correctly. Needs more work :)
app.post('/titanic_create', (req,res)  => {
  console.log("Trying to create a new user")  
  console.log ("Age: " + req.body.Age)
  const id = req.body.ID	
  const surv = req.body.Survived
  const pclass = req.body.Pclass
  const name = req.body.Name
  const sex = req.body.Sex
  const age = req.body.Age
  const sib = req.body.SiblingsSpouses_Aboard
  const par = req.body.ParentsChildren_Aboard
  const fare = req.body.Fare

  const queryString = "INSERT INTO titanic (ID,Survived,Pclass,Name,Sex,Age,SiblingsSpouses_Aboard,ParentsChildren_Aboard,Fare) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)"
  connection.query(queryString, [id, surv, pclass, name, sex, age, sib, par, fare], (err, results, fields) => {
//  connection.query('INSERT INTO titanic SET ?', req.body,
//	  function (err,result) {
	  if (err) {
	    console.log("Failed to insert new user: " + err)
	    res.sendStatus(500)
	    return
	  }
//res.send('User added: ' + result.insertedID)
	  console.log(req.body)
  console.log('Inserted new user with id: ' + results.insertedID)
  res.end()
  })	  
	res.end()
})


//Start the server and listen on port 3003
app.listen(3003, () => {
  console.log("Server is listening on 3003")
})
