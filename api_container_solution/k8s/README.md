Kubernetes deployment

Follow the below steps to automatically deploy the api-mysql deployments on a working kubernetes cluster:

#Deploys and configures the mysql database

kubectl apply -f db-deployment.yaml

#Exposes the db deployment via a ClusterIP service to provide an endpoint for the API

kubectl apply -f db-service.yaml

#Deploys the API server

kubectl apply -f api-deployment.yaml

#Expose the API service via a Load Balancer service to provide an external endpoint

kubectl apply -f api-service.yaml

Once the external ip has been provisioned for the API service, you can perform GET and POST requests by using the external IP and the server port (3003). 

Example: curl http://<external_api_IP>:3003/titanic/<Passenger_ID>