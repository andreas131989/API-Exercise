Chosen technology stack: mySQL for the Database and NodeJS for the API


In this repository you can find the following structure

- Docker-compose directory 

Includes a docker-compose file that automatically provisions three containers:

1) A containerized mysql database which creates a database after initialization

2) A containerized ansible controller which initially waits for 1m for mySQL container to initialize then connects securely via SSH to mysql container, creates a table, populates the database with the titanic.csv data and eventually terminates

3) A containerized API nodeJS server that performs HTTP GET and POST requests

You can start the process by going into the docker-compose directory and executing: docker-compose up -d

You can test GET requests by using the command: curl http://<container_api_IP>/titanic/<Passenger_ID> 

- Kubernetes directory

Includes yaml manifest files that provision:

1) A mysql pod and config map to configure the database (ansible is not used in this case)

2) An API pod that is exposed via service and is able to communicate with the database. More details on the k8s directory.

The images are pulled from a public docker hub registry: andreas131989


--------------------------------------------------------------------------------------


Time spent on developing both assets: ~9h

Three main improvements to be done: 1) Use DNS names over IPs (in the NodeJS server as well as for the http API calls), 2) Fix POST messages as currently wrong values are imported in the DB, 3) Having the ssh keys in a repo is definitely not ideal security wise, so a key store could be used (ansible vault, hashicorp vault,etc)